import java.net.Socket;
import java.net.ServerSocket;
import java.io.*;
import java.util.*;

public class ServerRunnable implements Runnable {

    private Socket socket;

    public ServerRunnable(Socket sk) {
        socket = sk;
    }

    public void run() {
        try (OutputStream outs = socket.getOutputStream();
                InputStream ins = socket.getInputStream();
                PrintWriter out = new PrintWriter(outs, true);
                Scanner in = new Scanner(ins);) {
            String inputLine;
            while (in.hasNextLine()) {
                inputLine = in.nextLine();
                System.out.println("Recivied message:" + inputLine + "from client: " + socket.toString());
                out.println(inputLine);
            }

        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port");
            System.out.println(e.getMessage());
        }

    }

}