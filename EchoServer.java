import java.net.Socket;
import java.net.ServerSocket;
import java.io.*;
import java.util.*;

public class EchoServer{
    public static void main(String[] args) {

        if (args.length != 1) {
            System.err.println("Usage: java EchoServer <port number>");
            System.exit(1);
        }

        final int PORT = Integer.parseInt(args[0]);

        while(true){
            try(
                ServerSocket serverSocket=new ServerSocket(PORT);
                Socket socket=serverSocket.accept();
                OutputStream outs= socket.getOutputStream();
                InputStream ins= socket.getInputStream();
                PrintWriter out=new PrintWriter(outs, true);
                Scanner in= new Scanner(ins);
            ){
                String inputLine;
                while(in.hasNextLine()){
                    inputLine=in.nextLine();
                    System.out.println("Received message : "+ inputLine+ " from client: "+socket.toString());
                    out.println(inputLine);
                }
            }catch(IOException e){
                System.out.println("Exception caught while trying to listen to port");
                System.out.println(e.getMessage());
                System.exit(1);
            }
        }
    }
}