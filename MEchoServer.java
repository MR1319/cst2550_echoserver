import java.net.Socket;
import java.net.ServerSocket;
import java.io.*;
import java.util.*;

public class MEchoServer {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Usage: java EchoServer <port number>");
            System.exit(1);
        }

        final int PORT = Integer.parseInt(args[0]);
        try {
            ServerSocket serverSocket = new ServerSocket(PORT);
            
            while (true) {
                Socket socket = serverSocket.accept();
                new Thread(new ServerRunnable(socket)).start();
            }
        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port");
            System.out.println(e.getMessage());
        }

    }
}